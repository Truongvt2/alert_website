import json
import os
import time, datetime
import requests
import smtplib
from email.message import EmailMessage
import urllib
import resources.settings as var
import subprocess
import tweepy

currDate = datetime.datetime.now()
currDate = str(currDate.strftime("%d-%m-%Y %H:%M:%S"))

def mail_alert(title, content):
        
        msg = EmailMessage()
        msg['Subject'] = title
        msg['From'] = var.fromaddr
        msg['To'] = var.toaddrs
        msg.set_content(content)

        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(var.fromaddr, var.password)
        server.send_message(msg)
        server.quit()

def update_twitter(content_twitter):
    consumer_key = var.consumer_key
    consumer_secret_key = var.consumer_secret_key
    auth = tweepy.OAuthHandler(consumer_key,consumer_secret_key)
    auth.set_access_token(var.access_token,var.access_token_secret)
    api = tweepy.API(auth)
    api.update_status(content_twitter)


def check_process():
      pytonProcess = subprocess.check_output("ps -ef | grep main.py",shell=True).decode()
      pytonProcess = pytonProcess.split('\n')
      string = 'python3 main.py'
      flag = 0
      for line in pytonProcess:
          if string in line:
              flag +=1
      return flag

def alert(url, log):
    string1 = 'down'
    string2 = 'up'
    filelog = open(log, "r")
    flag1 = 0
    flag2 = 0
    title = title_email(url)
    content_twitter = "Website " + url + " down at " + currDate + ", we will update again when it get back normally"
    content = check_status(url)
    for line in filelog:
        if string1 in line:
            flag1 = 1
        if string2 in line:
            flag2 = 2
            break
    if flag1 == 1:
        print('Exception: website: ', url,  'down before and down this time, wait timeout before continue sent')
        time.sleep(30)
        mail_alert(title, content)
    elif flag2 == 2:
        print('Exception: website: ', url,  'up before and down this time, send alert right now')
        mail_alert(title, content)
        print("go to update status in twitter")
        update_twitter(content_twitter)
        print("go to write log", url)
        with open(log, "w") as f:
             f.write("down")
             f.close()
             return
    else:
        print('Exception: website: ', url,  'down send alert email')
        mail_alert(title, content)
        print('\t', 'go to write log', url)
        with open(log, "w") as f:
             f.write('down')
             f.close()

def check_status(url):
    try:
        if url == var.url_list[2]:
            payload = open('resources/payload.json')
            param = json.load(payload)
            http_code = requests.post(url, data=param, timeout=10).status_code
            r = requests.post(url, data=param, timeout=10)
            respTime = str(round(r.elapsed.total_seconds(),3))
        else:
            http_code = requests.get(url, timeout=10).status_code
            r = requests.post(url, timeout=10)
            respTime = str(round(r.elapsed.total_seconds(),3))
        if (http_code == 200):
            return "Hi, " + os.linesep + os.linesep  + "Website " + url + " is backup UP again (HTTP 200-OK)" + os.linesep + "Start time: " + currDate + os.linesep + "Response time: " + respTime + "s" + os.linesep + os.linesep + "Sincerely,"
    except requests.exceptions.HTTPError as err01:
        return "Hi," + os.linesep + os.linesep +"HTTP error: " + os.linesep + str(err01) + "," + os.linesep + "Event timestamp: " + currDate + os.linesep + os.linesep +"Sincerely,"
    except requests.exceptions.ConnectionError as err02:
        return "Hi," + os.linesep + os.linesep + "Error connecting: " + os.linesep + str(err02) + "," + os.linesep + "Event timestamp: " + currDate + os.linesep + os.linesep + "Sincerely,"
    except requests.exceptions.Timeout as err03:
        return "Hi," + os.linesep + os.linesep + "Timeout error: " + os.linesep + str(err03) + "," + os.linesep + "Event timestamp: " + currDate + os.linesep + os.linesep + "Sincerely,"
    except requests.exceptions.RequestException as err04:
        return "Hi," + os.linesep + os.linesep + "Error: " + os.linesep + str(err04) + "," + os.linesep + "Event timestamp: " + currDate + os.linesep + os.linesep + "Sincerely,"

def title_email(url):
    try:
        if url == var.url_list[2]:
            payload = open('resources/payload.json')
            param = json.load(payload)
            http_code = requests.post(url, data=param, timeout=10).status_code
        else:
            http_code = requests.get(url, timeout=10).status_code
        if http_code == 200:
            return "Resolved: " + str(url) + " " + "up"
        else:
            return "Urgent: " + str(url) + " " + "down"
    except:
        return "Urgent: " + str(url) + " " + "down"

def send_email(url, log):
    try:
        string1 = 'down'
        string2 = 'up'
        filelog = open(log, "r")
        flag1 = 0
        flag2 = 0
        for line in filelog:
            if string1 in line:
                flag1 = 1
            elif string2 in line:
                flag2 = 2
                break
        if ((flag1 == 0) and (flag2 == 0)):
          print('This is the first time run script for web: ' ,url, 'go to write log')
          with open(log, "w") as f:
               f.write("up")
               f.close()
               return
        if url == var.url_list[2]:
            payload = open('resources/payload.json')
            param = json.load(payload)
            http_code = requests.post(url, data=param, timeout=10).status_code
            content_twitter = "Website " + url + " down at " + currDate + ", we will update again when it get back normally"
            if http_code == 200:
                r = requests.post(url, data=param, timeout=10)
                respTime = str(round(r.elapsed.total_seconds(),3))
                if flag1 == 1:
                    print('send resolved email:', url)
                    title = "Resolved: " + str(url) + " " + "up"
                    content = "Hi, " + os.linesep + os.linesep  + "Website " + url + " is backup UP again (HTTP 200-OK)" + os.linesep + "Start time: " + currDate + os.linesep + "Response time: " + respTime + "s" + os.linesep + os.linesep + "Sincerely,"
                    mail_alert(title, content)
                    print("go to update status in twitter")
                    content_twitter = "Website " + url + " get back normally at " + currDate
                    update_twitter(content_twitter)
                    with open(log, "w") as f:
                         f.write('up')
                         f.close()
                         return
                if flag2 == 2:
                    print( url, ' still up, no need to send email')
                    return
            else:
                request_content = requests.post(url, data=param, timeout=10).content
                title = "Urgent: " + str(url) + " " + "down"
                content = check_status(url)
                if flag1 == 1:
                    print('go to mail_alert function after interval time for:', url)
                    time.sleep(30)
                    mail_alert(title, content)
                    return
                if flag2 == 2:
                    print(url, 'up before, go to alert email:')
                    mail_alert(title, content)
                    print("go to update status in twitter")
                    update_twitter(content_twitter)
                    with open(log, "w") as f:
                         f.write('down')
                         f.close()
                         return
                return
        else:
            http_code = requests.get(url, timeout=10).status_code
            r = requests.post(url, timeout=10)
            respTime = str(round(r.elapsed.total_seconds(),3))
            print("debug:", respTime)
        if flag1 == 1:
            if http_code == 200:
                print('\t','send resolve email for: ', url)
                title = "Resolved: " + str(url) + " " + "up"
                content = "Hi, " + os.linesep + os.linesep  + "Website " + url + " is backup UP again (HTTP 200-OK)" + os.linesep + "Start time: " + currDate + os.linesep + "Response time: " + respTime + "s" + os.linesep + os.linesep + "Sincerely,"
                content_twitter = "Website " + url + " get back normally at" + currDate
                mail_alert(title, content)
                print("go to update status in twitter")
                update_twitter(content_twitter)
                print('\t','go to write log of', url)
                with open(log, "w") as f:
                     f.write('up')
                     f.close()
                     return
            else:
                print('\t','send alert down after interval for:', url)
                time.sleep(30)
                title = "Urgent: " + str(url) + " " + "down"
                content = check_status(url)
                mail_alert(title, content)
                return
        elif flag2 == 2:
            if http_code == 200:
                print(url, 'already up, no need to send email')
            else:
                print(url, 'still down, send email alert down after interval')
                time.sleep(30)
                title = "Urgent: " + str(url) + " " + "down"
                content = check_status(url)
                mail_alert(title, content)
                return
    except:
        return alert(url, log)
