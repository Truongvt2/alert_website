import multiprocessing
from pathlib import Path
from resources.functions import *
import urllib
import resources.settings as var
import subprocess

def main():
    p = check_process()
    if p > 4:
        exit(0)
    flag = 0
    jobs = []
    for url in var.url_list:
        log = "/tmp/log_checkurl_%s.txt" %flag
        touchlog = Path(log)
        touchlog.touch(exist_ok=True)
        p = multiprocessing.Process(target=send_email, args=(url, log, ))
        jobs.append(p)
        p.start()
        flag += 1

if __name__ == "__main__":
    main()

